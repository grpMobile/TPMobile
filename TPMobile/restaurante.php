<?php
try {
    $pdo = new PDO('sqlite:restaurante.sqlite3');
}
catch(PDOException $ex){
    die(json_encode(array('outcome' => false, 'message' => 'Unable to connect')));
}

function getMenu(){
    global $pdo;
    $sqlStatement = "SELECT * FROM categories INNER JOIN photos ON categories.photo_id = photos.id; ";
    $requete = $pdo->prepare($sqlStatement);

    $requete->execute();
    return $requete->fetchAll(PDO::FETCH_ASSOC);

}

function getMenuFromCategoryId($id){
    global $pdo;
    /*$sqlStatement = "SELECT allergies.id as allergie_id, allergies.nom as allergie_nom, plats.*, photos.path FROM (((allergies INNER JOIN allergie_plats ON allergies.id = allergie_plats.id_allergie) INNER JOIN plats ON allergie_plats.id_plat = plats.id) INNER JOIN plat_photos ON plats.id = plat_photos.id_plat) INNER JOIN photos ON plat_photos.id_photo = photos.id WHERE plats.categorie_id = :id";*/
    
    
    $sqlStatement = "SELECT * FROM plats WHERE plats.categorie_id = :category_id"
    $requete = $pdo->prepare($sqlStatement);
    $requete->bindValue(':id', $id);
    $requete->execute();
    $plats = $requete->fetchAll(PDO::FETCH_ASSOC);
    
    foreach($plats as $key=>$plat){
        $sqlStatement = "SELECT * FROM plat_photos WHERE plat_photos.id_plat = :id";
        $requete = $pdo->prepare($sqlStatement);
        $requete->bindValue(':id', $plats[$key]['id']);
        $requete->execute();
        $info = $requete->fetchAll(PDO::FETCH_ASSOC);
        $plats[$key]['photos'] = $info;
        
        
        $sqlStatement = "SELECT * FROM allergies INNER JOIN allergie_plats ON allergie.id = allergie_plats.id_allergie WHERE allergie_plats.id_plat = :id";
        $requete = $pdo->prepare($sqlStatement);
        $requete->bindValue(':id', $plats[$key]['id']);
        $requete->execute();
        $info = $requete->fetchAll(PDO::FETCH_ASSOC);
        $plats[$key]['allergie'] = $info;
    }
    
    return $plats;
}

function getDetailsFromPlatId($id){
    global $pdo;
    /*$sqlStatement = "SELECT commentaires.*, plats.id as plat_id, plats.nom FROM ((plats INNER JOIN plat_photos ON plats.id = plat_photos.id_plat) INNER JOIN photos ON plat_photos.id_photo = photos.id) INNER JOIN commentaires ON plats.id = commentaires.id_plat WHERE plats.id = :id;";*/
    
    $sqlStatement = "SELECT * FROM plats WHERE plats.id = :id"
    $requete = $pdo->prepare($sqlStatement);
    $requete->bindValue(':id', $id);
    $requete->execute();
    $plats = $requete->fetchAll(PDO::FETCH_ASSOC);
    
    foreach($plats as $key=>$plat){
        $sqlStatement = "SELECT * FROM plat_photos WHERE plat_photos.id_plat = :id";
        $requete = $pdo->prepare($sqlStatement);
        $requete->bindValue(':id', $id);
        $requete->execute();
        $info = $requete->fetchAll(PDO::FETCH_ASSOC);
        $plats[$key]['photos'] = $info;
        
        
        $sqlStatement = "SELECT * FROM commentaires WHERE commentaires.id_plat = :id";
        $requete = $pdo->prepare($sqlStatement);
        $requete->bindValue(':id', $id);
        $requete->execute();
        $info = $requete->fetchAll(PDO::FETCH_ASSOC);
        $plats[$key]['allergie'] = $info;
    }
    
    return $plats;

}

function orderPlat($data){
    global $pdo;
    $sqlStatement = "INSERT INTO commandes (date) VALUES ('".date('Y-m-d H:i:s', strtotime('now'))."');";
    $requete = $pdo->prepare($sqlStatement);
    $requete->execute();
    $commandeId = $pdo->lastInsertId();
    
    $sousTotal = 0;
    
    foreach($data as $plats){
        $id_plat = $plats['id'];
        $sqlStatement = "INSERT INTO repas_commandes (id_plat, id_commande) VALUES (:id_plat, :id_commande)";
        $requete = $pdo->prepare($sqlStatement);
        $requete->bindValue(':id_plat', $id_plat);
        $requete->bindValue(':id_commande', $commandeId);
        $requete->execute();
        $sousTotal += $plats['prix']; 
    }
    
    $taxe = $sousTotal * 0.14975;
    $total = $sousTotal + $taxe;
    
    $sqlStatement = "UPDATE commandes SET commandes.sous_total= ".$sousTotal.", commandes.total= ".$total.", commandes.taxe= ".$taxe." WHERE commandes.id= ".$commandeId;
    $requete = $pdo->prepare($sqlStatement);
    $requete->execute();
    
}

function addReview($data){
    $sqlStatement = "INSERT INTO commentaire (id_plat, commentaire, etoile) VALUES (:id_plat, :commentaire, :etoile)";
    $requete = $pdo->prepare($sqlStatement);
    $requete->bindValue(':id_plat', $data['id_plat']);
    $requete->bindValue(':id_commande', $data['id_commande']);
    $requete->bindValue(':etoile', $data['etoile']);
    $requete->execute();

    $sqlStatement = "UPDATE repas_commandes SET commentaire=1 WHERE repas_commandes.id_plat = :id_plat AND repas_commandes.id_commande= :id_commande";
    $requete = $pdo->prepare($sqlStatement);
    $requete->bindValue(':id_plat', $data['id_plat']);
    $requete->bindValue(':id_commande', $data['id_commande']);
    $requete->execute();
}

?>
